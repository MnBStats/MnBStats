"""TODO: Add documentation."""

import logging
from mnbparser.team import Teams, Team
from mnbmodel.db import session_scope
from mnbmodel.query import read


def create_teams(database, tournament_name, players, tags):
    with session_scope(database) as session:
        game_ids = read.tournament_game_ids(session, tournament_name)
        home_team = create_team(session, tags.home, tournament_name, game_ids, players)
        away_team = create_team(session, tags.away, tournament_name, game_ids, players)
        return Teams(home_team, away_team)


def create_team(session, tag, tournament_name, all_ids, players):
    team_game_ids = read.team_game_ids(session, tag, tournament_name)

    return Team(
        players=set(_validate_players(players, tag, team_game_ids, all_ids)),
        tag=tag)


def _validate_players(players, tag, team_game_ids, all_assigned_ids):
    for player in players:
        yield from _validate_player(
            player, tag, team_game_ids, all_assigned_ids)


def _validate_player(player, tag, team_game_ids, all_assigned_ids):
    if player.game_id in team_game_ids:
        yield player
    elif player.name.tag == tag:
        if player.game_id not in all_assigned_ids:
            yield player
        else:
            # TODO: Should throw exception?
            msg = "Player {0} assigned to different team".format(player)
            logging.error(msg)
