from mnbmodel import models as MDL
from mnbmodel.query import read

Kill = MDL.Kill
Assist = MDL.Assist
MatchPlayer = MDL.MatchPlayer
Spawn = MDL.Spawn


def add_kill(session, kill, match_id):
    kill = Kill(
        match_id=match_id,
        killer_id=read.player(session, kill.killer.game_id).player_id,
        victim_id=read.player(session, kill.victim.game_id).player_id,
        kill_type=kill.kill_type,
        spawn=kill.spawn_number,
        round=kill.round_number,
        kill_number=kill.kill_number,
        teamkill=kill.teamkill,
        weapon_id=kill.weapon_id)

    session.add(kill)


def add_assist(session, assist, db_match):
    assist = Assist(
        match=db_match,
        player=read.player(session, assist.player.game_id),
        dmg=assist.dmg,
        team_dmg=assist.team_dmg,
        number_of_assists=assist.number_of_assists,
        spawn=assist.spawn_number,
        round=assist.round_number)

    session.add(assist)


def add_match_player(session, spawn_info, db_match):
    match_player = MatchPlayer(
        match=db_match,
        player=read.player(session, spawn_info.player.game_id),
        side=spawn_info.side,
        wb_class=spawn_info.wb_class,
        spawn=spawn_info.spawn_number,
        round=spawn_info.round_number)

    session.add(match_player)


def add_spawn(session, spawn_number, spawn, db_match, tour_name):
    spawn = Spawn(
        match=db_match,
        spawn=spawn_number,
        map=spawn.map,
        attacking_faction=spawn.attacking_faction,
        attacking_team=read.team(session, spawn.attacking_team, tour_name).tournament_team_id,
        attacker_score=spawn.score.home_score,
        defender_score=spawn.score.away_score,
        defending_team=read.team(session, spawn.defending_team, tour_name).tournament_team_id,
        defending_faction=spawn.defending_faction,
        start_time=spawn.start_time)

    session.add(spawn)
