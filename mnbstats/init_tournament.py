import ruamel.yaml as yaml
from mnbmodel import db as DB
from mnbmodel.query import add, delete


def init(config):
    database = DB.Database(config.db_conn_string)

    with open(config.tour_config_file) as stream:
        try:
            tour = yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            print(exc)
            raise

    with DB.session_scope(database) as session:
        delete.tournament(session, tour["shortName"])

    with DB.session_scope(database) as session:
        add.tournament(
            session,
            tour["shortName"],
            tour["fullName"])

        for team in tour["teams"]:
            add.team(
                session,
                team["name"],
                team["tag"],
                tour["shortName"])

        rules = tour['rules']
        add.tournament_rules(
            session,
            tour["shortName"],
            rules['spawns'],
            rules['rounds_to_win'],
            rules['players_per_team'])
