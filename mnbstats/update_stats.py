import copy
import subprocess
from mnbparser import event_loader
from mnbparser.match import Match, trim_irrelevant_spawns
from mnbparser.spawn import Spawn
from mnbparser.team import Teams, Team
from mnbparser.match_rules import Rules
from mnbmodel import db as DB
from mnbmodel.db import session_scope
from mnbmodel.query import read
from mnbmodel.transactions import assign
from mnbmodel.transactions import prepare_match
from mnbstats import db
from mnbstats import console_output as console
from mnbstats.teams_selector import create_teams
from mnbstats.fixtures import load_fixtures


def update_stats(config):
    database = DB.Database(config.db_conn_string)

    matches = list(load_matches(database, config))
    for match in matches:
        with DB.session_scope(database) as session:
            tags = match.teams.tags
            console.print_match_header(tags)

            db_match = prepare_match.prepare_match(
                session, config.tour_name, config.week_num, tags.home,
                tags.away, match.score.home_score, match.score.away_score)

            assign_players_to_team(
                session, match.teams.home, config.tour_name)
            assign_players_to_team(
                session, match.teams.away, config.tour_name)

            for kill in match.kills:
                # TODO: reason for if: suicides not in database yet
                if kill.killer:
                    db.add_kill(session, kill, db_match.match_id)

            for assist in match.assists:
                db.add_assist(session, assist, db_match)

            for spawn_info in match.spawned_players:
                db.add_match_player(session, spawn_info, db_match)

            for spawn_number, spawn in enumerate(match.spawns, 1):
                db.add_spawn(session, spawn_number, spawn, db_match, config.tour_name)

            session.commit()


def load_matches(database, config):
    rules = load_tournament_rules(database, config.tour_name)
    for fixture in load_fixtures(config.match_dir):
        match = load_match_with_exception_check(
            database, fixture, rules, config.tour_name)

        console.print_status(match)
        while match.status == Match.Status.FAILURE:
            subprocess.Popen([config.text_editor, fixture.log])
            match = resolve_parsing_failure(
                database, match, fixture, rules, config.tour_name)

        match.teams = limit_teams_to_participants(
            match.teams, match.kills, match.spawned_players)

        yield match


def load_tournament_rules(database, tour_name):
    with session_scope(database) as session:
        tour = read.tournament(session, tour_name)
        return Rules(
            min_no_spawns=tour.spawns,
            rounds_to_win=tour.rounds_to_win,
            max_no_of_rounds=tour.rounds_to_win + tour.rounds_to_win - 1,
            no_of_kills=tour.players_per_team)


def load_match_with_exception_check(database, fixture, rules, tour_name):
    if fixture.is_exceptional():
        return load_exceptional_match(database, fixture, rules, tour_name)

    return load_match(database, fixture, rules, tour_name)


def load_exceptional_match(database, fixture, rules, tour_name):
    if fixture.exception_type == fixture.Exception.DEFAULT_WIN:
        match = load_match(database, fixture, rules, tour_name)
        apply_default_win(match, fixture.exception_data)
        console.default_win(match)
        return match

    if fixture.exception_type == fixture.Exception.DIFFERENT_TEAM_SIZE:
        new_rules = copy.copy(rules)
        new_rules.required_number_of_kills = fixture.exception_data.team_size
        match = load_match(database, fixture, new_rules, tour_name)
        console.different_team_size(match, new_rules.required_number_of_kills)
        return match

    if fixture.exception_type == fixture.Exception.TIE_BREAKER:
        new_rules = copy.copy(rules)
        new_rules.min_number_of_spawns = fixture.exception_data.number_of_spawns
        match = load_match(database, fixture, new_rules, tour_name)
        console.tie_breaker(match, new_rules.min_number_of_spawns)
        return match

    if fixture.exception_type == fixture.Exception.FUCK_UP:
        match = load_match(database, fixture, rules, tour_name)
        selected_spawns = [int(spawn_number)-1 for spawn_number in
                           fixture.exception_data.spawns.split(",")]
        match.spawns = [match.spawns[spawn_number] for spawn_number in selected_spawns]
        match.score = sum(s.score for s in match.spawns)
        match.status = Match.Status.SUCCESS
        console.spawns_preselected(match, fixture.exception_data.spawns)
        return match


def apply_default_win(match, score):
    match.status = Match.Status.SUCCESS
    match.score.home_score = score.home_score
    match.score.away_score = score.away_score


def load_match(database, fixture, rules, tour_name):
    players = event_loader.load_players(fixture.log, fixture.tags)
    teams = create_teams(database, tour_name, players, fixture.tags)
    events = event_loader.load_match_related_events(fixture.log, teams)
    return Match.create(events, teams, rules)


def resolve_parsing_failure(database, match, fixture, rules, tour_name):
    if more_then_4_spawns_accepted(match):
        match.spawns = trim_irrelevant_spawns(match.spawns, Match.Status.SUCCESS)

    console.match_analysis(match)
    option = console.print_menu()
    if option == 'a':
        exit()
    if option == 'r':
        return load_match(database, fixture, rules, tour_name)
    if option == 's':
        user_input = input("Choose spawns (eg. '1, 2, 3'): ").split(",")
        selected_spawns = [int(spawn_number)-1 for spawn_number in user_input]
        match.spawns = [match.spawns[spawn_number] for spawn_number in selected_spawns]
        match.status = Match.Status.SUCCESS
        return match


def more_then_4_spawns_accepted(match):
    return sum(s.status == Spawn.Status.ACCEPTED for s in match.spawns) > 4


def limit_teams_to_participants(teams, kills, spawned_players):
    return Teams(
        participants(teams.home, kills, spawned_players),
        participants(teams.away, kills, spawned_players))


def participants(team, kills, spawned_players):
    nicknames = set(gen_participants_from(team, kills, spawned_players))
    players = (player for player in team if player.name in nicknames)
    return Team(players, team.tag)


def gen_participants_from(team, kills, spawned_players):
    for kill in kills:
        if kill.killer is not None:
            if kill.killer.name in team:
                yield kill.killer.name
        if kill.victim.name in team:
            yield kill.victim.name

    for player in spawned_players:
        if player.player.name in team:
            yield player.player.name


def assign_players_to_team(session, team, tour_name):
    for player in team:
        "-"
        assign.assign_to_team(
            session,
            player.name.name,
            player.game_id,
            team.tag,
            tour_name)
