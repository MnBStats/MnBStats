import logging
from colorama import init
init(autoreset=True)
logging.basicConfig(filename='error.log', filemode='w', level=logging.WARNING)
