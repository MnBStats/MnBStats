import os
import ruamel.yaml as yaml
from mnbparser.team import Tags
from mnbparser.utils import AutoNumberEnum


class Score:
    def __init__(self, home_score, away_score):
        self.home_score = home_score
        self.away_score = away_score


class DifferentTeamSize:
    def __init__(self, team_size):
        self.team_size = team_size


class NumberOfSpawns:
    def __init__(self, number_of_spawns):
        self.number_of_spawns = number_of_spawns


class SpawnSelection:
    def __init__(self, spawns):
        self.spawns = spawns


class Fixture:
    class Exception(AutoNumberEnum):
        DEFAULT_WIN = ()
        DIFFERENT_TEAM_SIZE = ()
        TIE_BREAKER = ()
        FUCK_UP = ()

    def __init__(self, log, tags):
        self.log = log
        self.tags = tags
        self.exception_type = None
        self.exception_data = None

    def __repr__(self):
        return 'Fixture({})'.format(self.__dict__)

    def is_exceptional(self):
        return self.exception_type is not None


def load_fixtures(fixtures_path):
    exceptions = load_exceptions(fixtures_path)
    return list(load_fixtures_from_file_names(fixtures_path, exceptions))


def load_fixtures_from_file_names(fixtures_path, exceptions):
    all_files = os.listdir(fixtures_path)
    for f in (f for f in all_files if "VS" in f):
        log = fixtures_path + f
        tags = Tags(*f.split("VS"))
        fixture = Fixture(log, tags)

        for exc in exceptions['defaults']:
            if exc['home_tag'] == tags.home:
                fixture.exception_type = Fixture.Exception.DEFAULT_WIN
                fixture.exception_data = Score(
                    exc['home_score'], exc['away_score'])

        for exc in exceptions['different_team_size']:
            if exc['home_tag'] == tags.home:
                fixture.exception_type = Fixture.Exception.DIFFERENT_TEAM_SIZE
                fixture.exception_data = DifferentTeamSize(exc['team_size'])

        for exc in exceptions['tie_breaker']:
            if exc['home_tag'] == tags.home:
                fixture.exception_type = Fixture.Exception.TIE_BREAKER
                fixture.exception_data = NumberOfSpawns(exc['number_of_spawns'])

        for exc in exceptions['spawn_selection']:
            if exc['home_tag'] == tags.home:
                fixture.exception_type = Fixture.Exception.FUCK_UP
                fixture.exception_data = SpawnSelection(exc['spawns'])

        yield fixture


def load_exceptions(fixtures_path):
    try:
        with open(fixtures_path + 'exceptions.yml') as stream:
            try:
                return yaml.safe_load(stream)
            except yaml.YAMLError as exc:
                print(exc)
                raise
    except FileNotFoundError:
        return {
            'defaults': [],
            'different_team_size': [],
            'tie_breaker': [],
            'spawn_selection': []}
