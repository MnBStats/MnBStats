from colorama import Fore, Back
from mnbparser.match import Match
from mnbparser.utils import print_numbered_range


def print_status(match):
    color = Fore.GREEN if match.status == Match.Status.SUCCESS \
                       else Fore.RED

    print(color + "{:30}| {} | Score: {}-{}".format(
        str(match.teams.tags),
        match.status,
        match.score.home_score,
        match.score.away_score))


def default_win(match):
    color = Fore.MAGENTA
    print(color + "{:30}| is marked as default win, continue...".format(
        str(match.teams.tags)))


def different_team_size(match, required_number_of_kills):
    color = Fore.MAGENTA
    print(color + "{:30}| match with different team size: {}, continue...".format(
        str(match.teams.tags),
        required_number_of_kills))

def tie_breaker(match, number_of_spawns):
    color = Fore.MAGENTA
    print(color + "{:30}| tiebreaker match: {} spawns selected, continue...".format(
        str(match.teams.tags),
        number_of_spawns))


def spawns_preselected(match, spawns):
    color = Fore.MAGENTA
    print(color + "{:30}| match with pre-selected spawns: {}, continue...".format(
        str(match.teams.tags),
        spawns))


def print_summary(matches):
    num_of_failures = sum(m.match.status == Match.Status.FAILURE
                          for m in matches)

    print()
    if num_of_failures:
        print(Back.RED + "There are {} failures. Lets analyze them!"
              .format(num_of_failures))
    else:
        print("All fine. Lets proceed with database update!")
    print()


def match_analysis(match):
    print(Fore.YELLOW + "\n\nAnalysing match: {tags}\n"
          "Number of accepted spawns: {num}\n\n".format(
              tags=str(match.teams.tags),
              num=match.num_of_accepted_spawns))

    for spawn_num, spawn in enumerate(match.spawns, 1):
        if spawn.status.name == 'ACCEPTED':
            print("{0}. Spawn: start time: {1}, status: "
                      .format(spawn_num, spawn.start_time) +\
                  Fore.GREEN + "{0}"
                      .format(spawn.status.name) +\
                  Fore.WHITE + ", map: {0}, rounds:\n{1}"
                      .format(spawn.map, print_numbered_range(spawn.rounds, indent=2)))
        else:
            print("{0}. {1}".format(spawn_num, spawn))


def print_menu():
    print("Press (s) to select spawns manually, "
          "when you know which spawns are correct\n"
          "Press (a) to abort,\n"
          "Press (r) to reload log file.")
    return input()


def print_match_header(tags):
    print("\n----------------------\n "
          "| Teams: ", tags,
          "\n----------------------\n")
