import click
import ruamel.yaml as yaml
from mnbstats.update_stats import update_stats as update
from mnbstats.init_tournament import init


class Config:
    def __init__(self, db_conn_string, text_editor, logs_location, tour_name,
                 week_num):

        self.db_conn_string = db_conn_string
        self.text_editor = text_editor
        self.logs_location = logs_location
        self.tour_name = tour_name
        self.week_num = week_num

        self.tour_config_file = '{logs_location}{tour_name}/{tour_name}.yml'.format(
            logs_location=logs_location, tour_name=tour_name)
        self.tour_dir = '{logs_location}/{tour_name}/'.format(
            logs_location=logs_location, tour_name=tour_name)
        self.match_dir = '{tour_dir}/Week{week_num}/'.format(
            tour_dir=self.tour_dir, week_num=week_num)


def read_config(tour_name, week_num=0):
    with open('config.yml') as stream:
        try:
            c = yaml.safe_load(stream)
            return Config(
                c['db_conn_string'],
                c['text_editor'],
                c['logs_location'],
                tour_name,
                week_num)

        except yaml.YAMLError as exc:
            print(exc)
            raise


@click.command()
@click.argument('tour_name')
def init_tournament(tour_name):
    init(read_config(tour_name))


@click.command()
@click.argument('tour_name')
@click.argument('week_number')
def update_stats(tour_name, week_number):
    update(read_config(tour_name, week_number))
