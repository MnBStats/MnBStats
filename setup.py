from setuptools import setup, find_packages

setup(
    name='mnbstats',
    version='0.1',
    author="Piotr Wilk",
    license='MIT',
    packages=find_packages(exclude=['test*']),
    dependency_links=[
        'git+ssh://git@gitlab.com/MnBStats/mnbparser.git#egg=MnBParser',
        'git+ssh://git@gitlab.com/MnBStats/MnBModel.git#egg=MnBModel'
    ],
    test_suite="tests",
    install_requires=[
        'click',
        'colorama',
        'mnbmodel',
        'mnbparser',
        'ruamel.yaml',
        'SQLAlchemy',
        'mysql-connector-python'
    ],
    entry_points='''
        [console_scripts]
        init_tournament=mnbstats.scripts.run:init_tournament
        update_stats=mnbstats.scripts.run:update_stats
    ''',
)
