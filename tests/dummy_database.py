class DummySession:
    def commit(self):
        pass

    def rollback(self):
        pass

    def close(self):
        pass


class DummyDatabase:
    def connect(self):
        return DummySession()