import os
import contextlib
import unittest
from unittest.mock import patch
from mnbparser.team import Tags, Team, Teams
from mnbparser.player import Nickname, Player
from mnbstats.teams_selector import create_teams, create_team
from .dummy_database import DummyDatabase, DummySession


TOURNAMENT_NAME = "XXX"
TEAM_ID = 177
TAG = "TAG_"
TAG_B = "B_"


class TestBase(unittest.TestCase):
    def tearDown(self):
        self.silent_remove('error.log')

    def silent_remove(self, filename):
        with contextlib.suppress(FileNotFoundError):
            os.remove(filename)


@patch('mnbstats.teams_selector.read.team_game_ids')
class TeamSelectorTestSuite(TestBase):
    def expect_selection(self, team, all_ids_in_tour, players):
        self.assertEqual(
            team,
            create_team(
                DummySession(),
                TAG,
                TOURNAMENT_NAME,
                all_ids_in_tour,
                players))

    def test_no_match_when_player_has_no_tag_or_assigned_id(
            self, mock_team_game_ids):
        """Nickname is not selected when
        -it has not given tag
        -it is not connected to game id that is assigned to the team."""
        mock_team_game_ids.return_value = set()

        player_without_tag = \
            Player(name=Nickname(name="Boban", tag=""), game_id="1")

        self.expect_selection(
            team=Team(set(), TAG),
            all_ids_in_tour=set(),
            players=[player_without_tag])

    def test_select_players_with_tag(self, mock_team_game_ids):
        """Select nicknames with tags."""
        mock_team_game_ids.return_value = set()

        players = [
            Player(name=Nickname(name="Boban", tag=TAG), game_id=1),
            Player(name=Nickname(name="Nabob", tag=TAG), game_id=2)]

        self.expect_selection(
            team=Team(players=set(players), tag=TAG),
            all_ids_in_tour=set(),
            players=players)

    def test_do_not_duplicate_selected_players(self, mock_team_game_ids):
        """Select nicknames with tags. Do not duplicate players."""
        mock_team_game_ids.return_value = set()

        player = Player(name=Nickname(name="Boban", tag=TAG), game_id=1)

        self.expect_selection(
            team=Team(players={player}, tag=TAG),
            all_ids_in_tour=set(),
            players=[player, player])

    def test_select_players_assigned_to_the_team(
            self, mock_team_game_ids):
        """Select players with game id assigned to the team."""
        mock_team_game_ids.return_value = {1, 2}

        players = [
            Player(name=Nickname(name="Boban", tag=""), game_id=1),
            Player(name=Nickname(name="Nabob", tag=""), game_id=2)]

        self.expect_selection(
            team=Team(
                players=set(players),
                tag=TAG),
            all_ids_in_tour=set(),
            players=players)

    @patch('mnbstats.teams_selector.logging.error')
    def test_player_with_tag_not_selected_due_to_multiclanning(
            self, mock_logger, mock_team_game_ids):
        """Do not select player with tag whose id is in all ids container
        but not in team ids container. It means the player is already
        assigned to a different team."""
        mock_team_game_ids.return_value = set()

        assigned_id = 1
        player = Player(
            name=Nickname(name="Boban", tag=TAG),
            game_id=assigned_id)

        self.expect_selection(
            team=Team(players={}, tag=TAG),
            all_ids_in_tour={assigned_id},
            players=[player])

        self.assertTrue(mock_logger.called)


@patch('mnbstats.teams_selector.read.team_game_ids')
@patch('mnbstats.teams_selector.logging.error')
@patch('mnbstats.teams_selector.read.tournament_game_ids')
class TeamsSelectorTestSuite(TestBase):
    def test_select_valid_players_for_two_teams(
            self, mock_all_ids, mock_logger, mock_team_game_ids):
        # TODO: name those ids
        mock_all_ids.return_value = {3, 4, 5}

        random_dude = Player(Nickname(name="RandomDude", tag=""), game_id=99)
        player_tag_a = Player(Nickname(name="Gandalf", tag=TAG), game_id=1)
        player_tag_b = Player(Nickname(name="Saruman", tag=TAG_B), game_id=2)
        player_assigned_to_a = Player(Nickname(name="Rohimir", tag=""), game_id=3)
        player_assigned_to_b = Player(Nickname(name="UrukHai", tag=""), game_id=4)
        multiclanner = Player(Nickname(name="WormTongue", tag=TAG), game_id=5)

        players = [
            random_dude,
            player_tag_a,
            player_tag_b,
            player_assigned_to_a,
            player_assigned_to_b,
            multiclanner]

        mock_team_game_ids.side_effect = [{3}, {4}]

        expected_selection = Teams(
            Team(players={player_tag_a, player_assigned_to_a}, tag=TAG),
            Team(players={player_tag_b, player_assigned_to_b}, tag=TAG_B))

        self.assertEqual(
            expected_selection,
            create_teams(
                DummyDatabase(), TOURNAMENT_NAME, players, Tags(TAG, TAG_B)))

        assert 1 == mock_logger.call_count
        self.assertTrue(mock_logger.called)


if __name__ == '__main__':
    unittest.main()
